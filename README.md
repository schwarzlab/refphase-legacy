# RefPhase [LEGACY - please use current version linked below]

**This code base is unmaintained and as presented in Watkins et al. 2020, Nature.**

**For the current version, see [here](https://bitbucket.org/schwarzlab/refphase).**

**Collaboration between McGranahan (UCL London) and Schwarz (MDC Berlin) labs.**  
**Contact:**
**- Nicholas McGranahan: nicholas.mcgranahan.10@ucl.ac.uk**
**- Roland Schwarz: roland.schwarz@mdc-berlin.de**
**- Tom Watkins: tom.watkins@crick.ac.uk**

